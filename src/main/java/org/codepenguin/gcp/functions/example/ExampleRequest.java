/*
 * MIT License
 *
 * Copyright (c) 2021 Jorge Garcia - codepenguin.org
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.codepenguin.gcp.functions.example;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import java.io.Serializable;

import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ZERO;

/**
 * Request payload for the example.
 *
 * @author Jorge Garcia
 * @version 1.0.0
 * @since 11
 */
public final class ExampleRequest implements Serializable {

    private static final long serialVersionUID = 5754107417883792961L;

    private String name;
    private int numberA;
    private int numberB;

    /**
     * Constructor.
     */
    public ExampleRequest() {
        this(null, INTEGER_ZERO, INTEGER_ZERO);
    }

    /**
     * Constructor.
     *
     * @param name    the name.
     * @param numberA the number a.
     * @param numberB the number b.
     */
    ExampleRequest(String name, int numberA, int numberB) {
        this.name = name;
        this.numberA = numberA;
        this.numberB = numberB;
    }

    /**
     * Gets the name.
     *
     * @return the name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     *
     * @param name the name.
     */
    @SuppressWarnings("unused")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets the number a.
     *
     * @return the number a.
     */
    public int getNumberA() {
        return numberA;
    }

    /**
     * Sets the number a.
     *
     * @param numberA the number a.
     */
    @SuppressWarnings("unused")
    public void setNumberA(int numberA) {
        this.numberA = numberA;
    }

    /**
     * Gets the number b.
     *
     * @return the number b.
     */
    public int getNumberB() {
        return numberB;
    }

    /**
     * Sets the number b.
     *
     * @param numberB the number b.
     */
    @SuppressWarnings("unused")
    public void setNumberB(int numberB) {
        this.numberB = numberB;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ExampleRequest other = (ExampleRequest) o;
        return numberA == other.numberA && numberB == other.numberB && Objects.equal(name, other.name);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(name, numberA, numberB);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("name", name).add("numberA", numberA)
                .add("numberB", numberB).toString();
    }
}
