/*
 * MIT License
 *
 * Copyright (c) 2021 Jorge Garcia - codepenguin.org
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.codepenguin.gcp.functions.example;

import com.google.cloud.functions.HttpFunction;
import com.google.cloud.functions.HttpRequest;
import com.google.cloud.functions.HttpResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static java.net.HttpURLConnection.HTTP_OK;
import static org.codepenguin.gcp.functions.example.ContentType.JSON;

/**
 * Example of a HTTP Google Cloud Function.
 *
 * @author Jorge Garcia
 * @version 1.0.0
 * @since 11
 */
@SuppressWarnings("unused")
public final class GcpFunctionExample implements HttpFunction {

    private static final Logger LOGGER = Logger.getLogger(GcpFunctionExample.class.getName());

    private static final String TEXT = "Hello World of Software Architecture Group!";

    private final Gson gson;
    private final RequestBodyExampleRequestFunction requestFunction;

    /**
     * Constructor.
     */
    public GcpFunctionExample() {
        gson = new GsonBuilder().setPrettyPrinting().create();
        requestFunction = new RequestBodyExampleRequestFunction(gson);
    }

    /**
     * Constructor.
     *
     * @param gson            the gson converter.
     * @param requestFunction the request function.
     */
    GcpFunctionExample(Gson gson, RequestBodyExampleRequestFunction requestFunction) {
        this.gson = gson;
        this.requestFunction = requestFunction;
    }

    @Override
    public void service(HttpRequest request, HttpResponse response) throws Exception {
        final var builder = new Message.Builder(TEXT, request.getMethod());

        final Optional<ExampleRequest> exampleRequestOptional;
        try {
            exampleRequestOptional = requestFunction.apply(request);
        } catch (RuntimeException e) {
            LOGGER.log(Level.SEVERE, "Error getting the request body", e);
            response.setStatusCode(HTTP_INTERNAL_ERROR);
            writeResponse(gson, response, new ErrorPayload(e.getMessage()));
            return;
        }

        if (exampleRequestOptional.isPresent()) {
            final var exampleRequest = exampleRequestOptional.get();
            if (StringUtils.isBlank(exampleRequest.getName())) {
                response.setStatusCode(HTTP_BAD_REQUEST);
                writeResponse(gson, response, new ErrorPayload("You must add a value to the name attribute."));
                return;
            }

            builder.exampleResponse(new ExampleFunction().apply(exampleRequest));
        }

        final var queryParameters = request.getQueryParameters();
        if (Objects.nonNull(queryParameters) && !queryParameters.isEmpty()) {
            queryParameters.entrySet().stream().map(GcpFunctionExample::entryToPair).forEach(builder::parameter);
        }

        final var headers = request.getHeaders();
        if (Objects.nonNull(headers) && !headers.isEmpty()) {
            headers.entrySet().stream().map(GcpFunctionExample::entryToPair).forEach(builder::header);
        }

        response.setStatusCode(HTTP_OK);

        writeResponse(gson, response, builder.build());
    }

    private static Pair<String> entryToPair(final Map.Entry<String, List<String>> entry) {
        return new Pair<>(entry.getKey(), Arrays.toString(entry.getValue().toArray()));
    }

    private void writeResponse(final Gson gson, final HttpResponse response, final ResponsePayload payload)
            throws IOException {
        response.setContentType(JSON.getMimeType());

        var writer = new PrintWriter(response.getWriter());
        writer.write(gson.toJson(payload));
        writer.flush();
    }
}
