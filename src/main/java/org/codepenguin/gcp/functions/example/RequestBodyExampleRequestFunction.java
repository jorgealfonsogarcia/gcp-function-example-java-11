/*
 * MIT License
 *
 * Copyright (c) 2021 Jorge Garcia - codepenguin.org
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.codepenguin.gcp.functions.example;

import com.google.cloud.functions.HttpRequest;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.Optional;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ZERO;
import static org.codepenguin.gcp.functions.example.ContentType.JSON;

/**
 * Function to convert an {@link HttpRequest} payload body into an {@link ExampleRequest}, only if there is a request
 * body.
 *
 * @author Jorge Garcia.
 * @version 1.0.0
 * @since 11
 */
class RequestBodyExampleRequestFunction implements Function<HttpRequest, Optional<ExampleRequest>> {

    private static final Logger LOGGER = Logger.getLogger(RequestBodyExampleRequestFunction.class.getName());

    private final Gson gson;

    /**
     * Constructor.
     *
     * @param gson the gson converter.
     */
    RequestBodyExampleRequestFunction(final Gson gson) {
        this.gson = gson;
    }

    @Override
    public Optional<ExampleRequest> apply(HttpRequest httpRequest) {
        if (httpRequest.getContentLength() == INTEGER_ZERO) {
            return Optional.empty();
        }

        final var contentType = httpRequest.getContentType();
        if (contentType.isEmpty()) {
            return Optional.empty();
        }

        if (!contentType.get().equalsIgnoreCase(JSON.getMimeType())) {
            return Optional.empty();
        }

        try {
            return Optional.of(gson.fromJson(httpRequest.getReader(), ExampleRequest.class));
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Error with ExampleRequest", e);
            return Optional.empty();
        }
    }
}
