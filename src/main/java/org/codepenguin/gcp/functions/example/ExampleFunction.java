/*
 * MIT License
 *
 * Copyright (c) 2021 Jorge Garcia - codepenguin.org
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.codepenguin.gcp.functions.example;

import java.util.function.Function;

import static java.lang.String.format;

/**
 * Function to convert an {@link ExampleRequest} into an {@link ExampleResponse}. The response contains a hello message
 * and the sum of the two received values.
 *
 * @author Jorge Garcia.
 * @version 1.0.0
 * @since 11
 */
class ExampleFunction implements Function<ExampleRequest, ExampleResponse> {

    private static final String MESSAGE_FORMAT = "Hello, %s! Here is your sum result.";

    @Override
    public ExampleResponse apply(ExampleRequest request) {
        return new ExampleResponse(format(MESSAGE_FORMAT, request.getName()),
                request.getNumberA() + request.getNumberB());
    }
}
