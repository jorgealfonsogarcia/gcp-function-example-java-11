/*
 * MIT License
 *
 * Copyright (c) 2021 Jorge Garcia - codepenguin.org
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.codepenguin.gcp.functions.example;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import java.io.Serializable;

/**
 * Response payload for the example.
 *
 * @author Jorge Garcia
 * @version 1.0.0
 * @since 11
 */
class ExampleResponse implements Serializable {

    private static final long serialVersionUID = -894848710214459615L;

    private final String message;
    private final int result;

    /**
     * Constructor.
     *
     * @param message the message.
     * @param result  the result.
     */
    ExampleResponse(String message, int result) {
        this.message = message;
        this.result = result;
    }

    /**
     * Gets the message.
     *
     * @return the message.
     */
    String getMessage() {
        return message;
    }

    /**
     * Gets the result.
     *
     * @return the result.
     */
    int getResult() {
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ExampleResponse other = (ExampleResponse) o;
        return result == other.result && Objects.equal(message, other.message);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(message, result);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("message", message).add("result", result)
                .toString();
    }
}
