/*
 * MIT License
 *
 * Copyright (c) 2021 Jorge Garcia - codepenguin.org
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.codepenguin.gcp.functions.example;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Represents the message to write as response of the cloud function.
 *
 * @author Jorge Garcia
 * @version 1.0.0
 * @since 11
 */
@SuppressWarnings("PMD.MissingStaticMethodInNonInstantiatableClass")
final class Message implements ResponsePayload {

    private final String text;
    private final String method;
    private final String datetime;
    private final ExampleResponse exampleResponse;
    private final Collection<Pair<String>> parameters;
    private final Collection<Pair<String>> headers;

    private Message(String text, String method, String datetime, ExampleResponse exampleResponse,
                    Collection<Pair<String>> parameters, Collection<Pair<String>> headers) {
        this.text = text;
        this.method = method;
        this.datetime = datetime;
        this.exampleResponse = exampleResponse;
        this.parameters = parameters;
        this.headers = headers;
    }

    /**
     * Gets the text.
     *
     * @return the text.
     */
    String getText() {
        return text;
    }

    /**
     * Gets the HTTP request method.
     *
     * @return the method.
     */
    String getMethod() {
        return method;
    }

    /**
     * Gets the datetime.
     *
     * @return the datetime.
     */
    String getDatetime() {
        return datetime;
    }

    /**
     * Gets the example response.
     *
     * @return the response.
     */
    ExampleResponse getExampleResponse() {
        return exampleResponse;
    }

    /**
     * Gets the collection of request's parameters.
     *
     * @return the parameters.
     */
    Collection<Pair<String>> getParameters() {
        return parameters;
    }

    /**
     * Gets the collection of request's headers.
     *
     * @return the headers.
     */
    Collection<Pair<String>> getHeaders() {
        return headers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Message other = (Message) o;
        return Objects.equal(text, other.text) && Objects.equal(method, other.method)
                && Objects.equal(datetime, other.datetime) && Objects.equal(exampleResponse, other.exampleResponse)
                && Objects.equal(parameters, other.parameters) && Objects.equal(headers, other.headers);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(text, method, datetime, exampleResponse, parameters, headers);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).add("message", text).add("method", method)
                .add("datetime", datetime).add("exampleResponse", exampleResponse)
                .add("parameters", parameters).add("headers", headers).toString();
    }

    /**
     * Builds a new instance of {@link Message}.
     */
    static class Builder {

        @SuppressWarnings("SpellCheckingInspection")
        private static final String DATETIME_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";

        private final String text;
        private final String method;
        private ExampleResponse exampleResponse;
        private final Collection<Pair<String>> parameters = new ArrayList<>();
        private final Collection<Pair<String>> headers = new ArrayList<>();

        /**
         * Constructor.
         *
         * @param text   the text.
         * @param method t
         */
        Builder(String text, String method) {
            this.text = text;
            this.method = method;
        }

        /**
         * Sets the example response.
         *
         * @param exampleResponse the example response.
         */
        void exampleResponse(final ExampleResponse exampleResponse) {
            this.exampleResponse = exampleResponse;
        }

        /**
         * Adds a parameter.
         *
         * @param parameter the parameter.
         */
        void parameter(final Pair<String> parameter) {
            parameters.add(parameter);
        }

        /**
         * Adds a header.
         *
         * @param header the header.
         */
        void header(final Pair<String> header) {
            headers.add(header);
        }

        /**
         * Build the instance.
         *
         * @return the instance.
         */
        Message build() {
            return new Message(text, method, datetime(), exampleResponse, parameters, headers);
        }

        private String datetime() {
            return new SimpleDateFormat(DATETIME_PATTERN).format(new Date());
        }
    }
}
