/*
 * MIT License
 *
 * Copyright (c) 2021 Jorge Garcia - codepenguin.org
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.codepenguin.gcp.functions.example;

import com.google.cloud.functions.HttpRequest;
import com.google.gson.Gson;
import org.codepenguin.util.JsonUtil;
import org.codepenguin.util.JsonUtilException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.Optional;

import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ONE;
import static org.apache.commons.lang3.math.NumberUtils.LONG_ZERO;
import static org.codepenguin.gcp.functions.example.ContentType.JSON;
import static org.codepenguin.util.JsonUtil.getResourceContent;
import static org.codepenguin.util.ResourcePathHelper.MOCK_REQUEST_JSON_PATH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

/**
 * Unit tests for {@link RequestBodyExampleRequestFunction}.
 *
 * @author Jorge Garcia
 * @version 1.0.0
 * @since 11
 */
@RunWith(JUnit4.class)
public class RequestBodyExampleRequestFunctionTest {

    private static final long CONTENT_LENGTH = 68L;

    @Mock
    private HttpRequest requestMock;

    private final RequestBodyExampleRequestFunction function = new RequestBodyExampleRequestFunction(new Gson());

    @Before
    public void setUp() {
        openMocks(this);
    }

    @Test
    public void givenRequestContentLengthZeroWhenApplyThenReturnEmpty() {
        when(requestMock.getContentLength()).thenReturn(LONG_ZERO);

        final var response = function.apply(requestMock);
        assertNotNull(response);
        assertTrue(response.isEmpty());

        verify(requestMock, times(INTEGER_ONE)).getContentLength();
    }

    @Test
    public void givenRequestContentTypeEmptyWhenApplyThenReturnEmpty() {
        when(requestMock.getContentLength()).thenReturn(CONTENT_LENGTH);
        when(requestMock.getContentType()).thenReturn(Optional.empty());

        final var response = function.apply(requestMock);
        assertNotNull(response);
        assertTrue(response.isEmpty());

        verify(requestMock, times(INTEGER_ONE)).getContentLength();
        verify(requestMock, times(INTEGER_ONE)).getContentType();
    }

    @Test
    public void givenRequestContentTypeIsNotJsonWhenApplyThenReturnEmpty() {
        when(requestMock.getContentLength()).thenReturn(CONTENT_LENGTH);
        when(requestMock.getContentType()).thenReturn(Optional.of("other"));

        final var response = function.apply(requestMock);
        assertNotNull(response);
        assertTrue(response.isEmpty());

        verify(requestMock, times(INTEGER_ONE)).getContentLength();
        verify(requestMock, times(INTEGER_ONE)).getContentType();
    }

    @Test
    public void givenRequestWithErrorWhenApplyThenReturnEmpty() throws IOException {
        when(requestMock.getContentLength()).thenReturn(CONTENT_LENGTH);
        when(requestMock.getContentType()).thenReturn(Optional.of(JSON.getMimeType()));
        when(requestMock.getReader()).thenThrow(IOException.class);

        final var response = function.apply(requestMock);
        assertNotNull(response);
        assertTrue(response.isEmpty());

        verify(requestMock, times(INTEGER_ONE)).getContentLength();
        verify(requestMock, times(INTEGER_ONE)).getContentType();
        verify(requestMock, times(INTEGER_ONE)).getReader();
    }

    @Test
    public void givenRequestWhenApplyThenSuccess() throws IOException, JsonUtilException {
        when(requestMock.getContentLength()).thenReturn(CONTENT_LENGTH);
        when(requestMock.getContentType()).thenReturn(Optional.of(JSON.getMimeType()));

        final var json = getResourceContent(MOCK_REQUEST_JSON_PATH);
        final var stringReader = new StringReader(json);
        final var bufferedReader = new BufferedReader(stringReader);
        when(requestMock.getReader()).thenReturn(bufferedReader);

        final var expected = JsonUtil.fromJson(json, ExampleRequest.class);

        final var response = function.apply(requestMock);
        assertNotNull(response);
        assertTrue(response.isPresent());

        final var exampleRequest = response.get();
        assertEquals(expected.getName(), exampleRequest.getName());
        assertEquals(expected.getNumberA(), exampleRequest.getNumberA());
        assertEquals(expected.getNumberB(), exampleRequest.getNumberB());

        verify(requestMock, times(INTEGER_ONE)).getContentLength();
        verify(requestMock, times(INTEGER_ONE)).getContentType();
        verify(requestMock, times(INTEGER_ONE)).getReader();
    }
}