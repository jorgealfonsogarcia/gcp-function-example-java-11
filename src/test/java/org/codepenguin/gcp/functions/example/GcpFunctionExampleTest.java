/*
 * MIT License
 *
 * Copyright (c) 2021 Jorge Garcia - codepenguin.org
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.codepenguin.gcp.functions.example;

import com.google.cloud.functions.HttpRequest;
import com.google.cloud.functions.HttpResponse;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static java.net.HttpURLConnection.HTTP_BAD_REQUEST;
import static java.net.HttpURLConnection.HTTP_INTERNAL_ERROR;
import static java.net.HttpURLConnection.HTTP_OK;
import static org.apache.commons.collections.CollectionUtils.isEqualCollection;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import static org.apache.commons.lang3.math.NumberUtils.INTEGER_ONE;
import static org.codepenguin.util.JsonUtil.fromJson;
import static org.codepenguin.util.JsonUtil.fromResourceContent;
import static org.codepenguin.util.ResourcePathHelper.MOCK_REQUEST_JSON_PATH;
import static org.codepenguin.util.ResourcePathHelper.MOCK_RESPONSE_BAD_REQUEST_NO_NAME_JSON_PATH;
import static org.codepenguin.util.ResourcePathHelper.MOCK_RESPONSE_INTERNAL_ERROR_JSON_PATH;
import static org.codepenguin.util.ResourcePathHelper.MOCK_RESPONSE_ONLY_WITH_EXAMPLE_REQUEST_JSON;
import static org.codepenguin.util.ResourcePathHelper.MOCK_RESPONSE_WITHOUT_PARAMETERS_AND_HEADERS_JSON_PATH;
import static org.codepenguin.util.ResourcePathHelper.MOCK_RESPONSE_WITH_PARAMETERS_AND_HEADERS_JSON_PATH;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

/**
 * Unit tests for {@link GcpFunctionExample}.
 *
 * @author Jorge Garcia
 * @version 1.0.0
 * @since 11
 */
@SuppressWarnings("PMD.SignatureDeclareThrowsException")
@RunWith(JUnit4.class)
public class GcpFunctionExampleTest {

    private static final String GET_METHOD = "GET";
    private static final String ERROR_MESSAGE = "Error Message";

    private final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    @Mock
    private HttpRequest requestMock;

    @Mock
    private HttpResponse responseMock;

    @Mock
    private RequestBodyExampleRequestFunction requestFunctionMock;

    private StringWriter stringWriter;
    private BufferedWriter bufferedWriter;

    private GcpFunctionExample example;

    @Before
    public void setUp() throws IOException {
        openMocks(this);

        when(requestMock.getMethod()).thenReturn(GET_METHOD);

        stringWriter = new StringWriter();
        bufferedWriter = new BufferedWriter(stringWriter);
        when(responseMock.getWriter()).thenReturn(bufferedWriter);

        example = new GcpFunctionExample();
        example = new GcpFunctionExample(gson, requestFunctionMock);
    }

    @After
    public void tearDown() throws Exception {
        verify(requestMock, times(INTEGER_ONE)).getMethod();
        verify(responseMock, times(INTEGER_ONE)).getWriter();

        if (Objects.nonNull(bufferedWriter)) {
            bufferedWriter.close();
        }

        if (Objects.nonNull(stringWriter)) {
            stringWriter.close();
        }
    }

    @Test
    public void givenRequestWithErrorWhenServiceThenShouldReturnInternalError() throws Exception {
        when(requestFunctionMock.apply(any(HttpRequest.class))).thenThrow(new RuntimeException(ERROR_MESSAGE));
        doNothing().when(responseMock).setStatusCode(HTTP_INTERNAL_ERROR);

        example.service(requestMock, responseMock);

        bufferedWriter.flush();

        final var expected = fromResourceContent(MOCK_RESPONSE_INTERNAL_ERROR_JSON_PATH, ErrorPayload.class);
        assertNotNull(expected);

        final var result = fromJson(stringWriter.toString(), ErrorPayload.class);
        assertNotNull(result);

        assertEquals(expected, result);

        verify(requestFunctionMock, times(INTEGER_ONE)).apply(any(HttpRequest.class));
        verify(responseMock, times(INTEGER_ONE)).setStatusCode(HTTP_INTERNAL_ERROR);
    }

    @Test
    public void givenRequestWithPayloadButNoNameWhenServiceThenReturnBadRequest() throws Exception {
        when(requestFunctionMock.apply(any(HttpRequest.class))).thenReturn(Optional.of(new ExampleRequest()));
        doNothing().when(responseMock).setStatusCode(HTTP_BAD_REQUEST);

        example.service(requestMock, responseMock);

        bufferedWriter.flush();

        final var expected = fromResourceContent(MOCK_RESPONSE_BAD_REQUEST_NO_NAME_JSON_PATH, ErrorPayload.class);
        assertNotNull(expected);

        final var result = fromJson(stringWriter.toString(), ErrorPayload.class);
        assertNotNull(result);

        assertEquals(expected, result);

        verify(requestFunctionMock, times(INTEGER_ONE)).apply(any(HttpRequest.class));
        verify(responseMock, times(INTEGER_ONE)).setStatusCode(HTTP_BAD_REQUEST);
    }

    @Test
    public void givenRequestWithPayloadWhenServiceThenShouldSuccess() throws Exception {
        final var exampleRequest = fromResourceContent(MOCK_REQUEST_JSON_PATH, ExampleRequest.class);
        assertNotNull(exampleRequest);

        when(requestFunctionMock.apply(any(HttpRequest.class))).thenReturn(Optional.of(exampleRequest));
        doNothing().when(responseMock).setStatusCode(HTTP_OK);

        example.service(requestMock, responseMock);

        bufferedWriter.flush();

        final var expected = fromResourceContent(MOCK_RESPONSE_ONLY_WITH_EXAMPLE_REQUEST_JSON, Message.class);
        assertNotNull(expected);

        final var result = fromJson(stringWriter.toString(), Message.class);
        assertNotNull(result);

        assertEquals(expected.getText(), result.getText());
        assertTrue(isNotBlank(result.getDatetime()));
        assertEquals(expected.getMethod(), result.getMethod());
        assertEquals(expected.getExampleResponse(), result.getExampleResponse());
        assertTrue(isEqualCollection(expected.getParameters(), result.getParameters()));
        assertTrue(isEqualCollection(expected.getHeaders(), result.getHeaders()));

        verify(requestFunctionMock, times(INTEGER_ONE)).apply(any(HttpRequest.class));
        verify(responseMock, times(INTEGER_ONE)).setStatusCode(HTTP_OK);
    }

    @Test
    public void givenEmptyParametersAndEmptyHeadersWhenServiceThenShouldSuccess() throws Exception {
        doNothing().when(responseMock).setStatusCode(HTTP_OK);

        example.service(requestMock, responseMock);

        bufferedWriter.flush();

        final var expected = fromResourceContent(MOCK_RESPONSE_WITHOUT_PARAMETERS_AND_HEADERS_JSON_PATH,
                Message.class);
        assertNotNull(expected);

        final var result = fromJson(stringWriter.toString(), Message.class);
        assertNotNull(result);

        assertEquals(expected.getText(), result.getText());
        assertTrue(isNotBlank(result.getDatetime()));
        assertEquals(expected.getMethod(), result.getMethod());
        assertEquals(expected.getExampleResponse(), result.getExampleResponse());
        assertTrue(isEqualCollection(expected.getParameters(), result.getParameters()));
        assertTrue(isEqualCollection(expected.getHeaders(), result.getHeaders()));

        verify(responseMock, times(INTEGER_ONE)).setStatusCode(HTTP_OK);
    }

    @Test
    public void givenNullParametersAndNullHeadersWhenServiceThenShouldSuccess() throws Exception {
        when(requestMock.getQueryParameters()).thenReturn(null);
        when(requestMock.getHeaders()).thenReturn(null);
        doNothing().when(responseMock).setStatusCode(HTTP_OK);

        example.service(requestMock, responseMock);

        bufferedWriter.flush();

        final var expected = fromResourceContent(MOCK_RESPONSE_WITHOUT_PARAMETERS_AND_HEADERS_JSON_PATH,
                Message.class);
        assertNotNull(expected);

        final var result = fromJson(stringWriter.toString(), Message.class);
        assertNotNull(result);

        assertEquals(expected.getText(), result.getText());
        assertTrue(isNotBlank(result.getDatetime()));
        assertEquals(expected.getMethod(), result.getMethod());
        assertEquals(expected.getExampleResponse(), result.getExampleResponse());
        assertTrue(isEqualCollection(expected.getParameters(), result.getParameters()));
        assertTrue(isEqualCollection(expected.getHeaders(), result.getHeaders()));

        verify(requestMock, times(INTEGER_ONE)).getQueryParameters();
        verify(requestMock, times(INTEGER_ONE)).getHeaders();
        verify(responseMock, times(INTEGER_ONE)).setStatusCode(HTTP_OK);
    }

    @Test
    public void givenParametersAndHeadersWhenServiceThenShouldSuccess() throws Exception {
        when(requestMock.getQueryParameters()).thenReturn(Map.of("parameter1", List.of("value1"),
                "parameter2", List.of("value2", "value3")));
        when(requestMock.getHeaders()).thenReturn(Map.of("header1", List.of("value4"),
                "header2", List.of("value5", "value6")));
        doNothing().when(responseMock).setStatusCode(HTTP_OK);

        example.service(requestMock, responseMock);

        bufferedWriter.flush();

        final var expected = fromResourceContent(MOCK_RESPONSE_WITH_PARAMETERS_AND_HEADERS_JSON_PATH, Message.class);
        assertNotNull(expected);

        final var result = fromJson(stringWriter.toString(), Message.class);
        assertNotNull(result);

        assertEquals(expected.getText(), result.getText());
        assertTrue(isNotBlank(result.getDatetime()));
        assertEquals(expected.getMethod(), result.getMethod());
        assertEquals(expected.getExampleResponse(), result.getExampleResponse());
        assertTrue(isEqualCollection(expected.getParameters(), result.getParameters()));
        assertTrue(isEqualCollection(expected.getHeaders(), result.getHeaders()));

        verify(requestMock, times(INTEGER_ONE)).getQueryParameters();
        verify(requestMock, times(INTEGER_ONE)).getHeaders();
        verify(responseMock, times(INTEGER_ONE)).setStatusCode(HTTP_OK);
    }
}