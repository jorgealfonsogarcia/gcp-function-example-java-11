/*
 * MIT License
 *
 * Copyright (c) 2021 Jorge Garcia - codepenguin.org
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.codepenguin.util;

/**
 * Resource paths for unit tests.
 *
 * @author Jorge Garcia
 * @version 1.0.0
 * @since 11
 */
public final class ResourcePathHelper {
    /**
     * Mock request.
     */
    public static final String MOCK_REQUEST_JSON_PATH = "mock-request.json";

    /**
     * Mock response bad request without name.
     */
    public static final String MOCK_RESPONSE_BAD_REQUEST_NO_NAME_JSON_PATH = "mock-response-bad-request-no-name.json";

    /**
     * Mock response internal error.
     */
    public static final String MOCK_RESPONSE_INTERNAL_ERROR_JSON_PATH = "mock-response-internal-error.json";

    /**
     * Mock response only with example request.
     */
    public static final String MOCK_RESPONSE_ONLY_WITH_EXAMPLE_REQUEST_JSON
            = "mock-response-only-with-example-request.json";

    /**
     * Mock response without parameters and headers.
     */
    public static final String MOCK_RESPONSE_WITHOUT_PARAMETERS_AND_HEADERS_JSON_PATH
            = "mock-response-without-parameters-and-headers.json";

    /**
     * Mock response with parameters and headers.
     */
    public static final String MOCK_RESPONSE_WITH_PARAMETERS_AND_HEADERS_JSON_PATH
            = "mock-response-with-parameters-and-headers.json";

    private ResourcePathHelper() {
    }
}
